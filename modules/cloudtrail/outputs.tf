output "bucket_arn" {
  value       = aws_s3_bucket.ct_bucket.arn
  description = "The ARN of the bucket. Will be of format arn:aws:s3:::bucketname."
}

output "bucket_name" {
  value       = aws_s3_bucket.ct_bucket.id
  description = "The name of the bucket."
}

output "bucket_region" {
  value       = aws_s3_bucket.ct_bucket.region
  description = "The AWS region this bucket resides in."
}

output "cloudtrail_arn" {
  value       = aws_cloudtrail.ct.arn
  description = "The ARN of the cloudtrail. Will be of format arn:aws:cloudtrail:::cloudtrail_name."
}

output "cloudtrail_region" {
  value       = aws_cloudtrail.ct.home_region
  description = "The region of the cloudtrail."
}

output "cloudtrail_multiregion" {
  value       = aws_cloudtrail.ct.arn
  description = "Show true if the service is multi organization"
}

output "cloudtrail_multiorg" {
  value       = aws_cloudtrail.ct.is_organization_trail
  description = "Show true if the service is across multiples region."
}