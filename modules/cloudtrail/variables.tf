variable "prefix" {
  type        = string
  description = "Prefix for the service"
}

variable "cloudtrail_name" {
  type        = string
  description = "Name of the cloudtrail service"
}

variable "cloudtrail_bucket" {
  type        = string
  description = "Name of the S3 bucket"
}

variable "is_multiregion" {
  type        = bool
  description = "Specify is the trail is multiregion the default is "
}

variable "is_org_trail" {
  type        = bool
  description = "Specify is the trail is multiregion the default is "
}