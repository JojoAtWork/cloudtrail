terraform {
  required_version = ">= 1.0"
}

data "aws_caller_identity" "current" {}


resource "aws_cloudtrail" "ct" {
  name                          = var.cloudtrail_name
  s3_bucket_name                = aws_s3_bucket.ct_bucket.id
  s3_key_prefix                 = var.prefix
  include_global_service_events = var.is_multiregion
  is_multi_region_trail         = var.is_multiregion
  is_organization_trail         = var.is_org_trail
  depends_on                    = [aws_s3_bucket.ct_bucket]
}

resource "aws_s3_bucket" "ct_bucket" {
  bucket        = var.cloudtrail_bucket
  force_destroy = true

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::${var.cloudtrail_bucket}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${var.cloudtrail_bucket}/${var.prefix}/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}