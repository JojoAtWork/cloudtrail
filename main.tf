module "common" {
  source = "./modules/common"
}

module "cloudtrail" {
  source                        = "./modules/cloudtrail"
  name                          = var.cloudtrail_name_ro
  bucket                        = var.cloudtrail_bucket_ro
  s3_key_prefix                 = var.prefix_ro
  include_global_service_events = var.is_multiregion_ro
  is_multi_region_trail         = var.is_multiregion_ro
  is_organization_trail         = var.is_org_trail_ro
}